use reqwest;

#[derive(Deserialize)]
pub struct Result {
    pub token: String,
    pub permissions: Vec<String>,
}

#[derive(Serialize)]
pub struct AppPayload {
    pub name: String,
    pub id: String,
    pub version: String,
    pub vendor: String,
}

#[derive(Serialize)]
pub struct Payload {
    pub app: AppPayload,
    pub permissions: Vec<String>,
}

pub fn request(payload: Payload) -> Result {
    let client = reqwest::Client::new().expect("Couldn't create client");
    let request = client.post("http://localhost:8100/auth").json(&payload);
    let mut response = request.send().expect("Failed to read response");
    response.json::<Result>().expect("Error deserializing the response.")
}
