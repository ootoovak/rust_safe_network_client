use reqwest;
use reqwest::header::Authorization;

pub fn request(token: String) -> String {
    let client = reqwest::Client::new().expect("Couldn't create client");
    let request = client.get("http://localhost:8100/auth").header(Authorization(token));
    let response = request.send().expect("Failed to read response");
    response.status().to_string()
}
