pub mod authorize_app;
pub mod validate_app_authorization;
pub mod revoke_app;

#[cfg(test)]
mod tests {
    use authorization::authorize_app;
    use authorization::validate_app_authorization;
    use authorization::revoke_app;

    #[test]
    fn it_works() {
        let app_payload = authorize_app::AppPayload {
            name: "SAFE Network Client".to_string(),
            id: "com.forefrontprojects.safenetworkclient".to_string(),
            version: "0.0.1".to_string(),
            vendor: "Forefront Projects".to_string(),
        };
        let payload = authorize_app::Payload {
            app: app_payload,
            permissions: vec!["SAFE_DRIVE_ACCESS".to_string()],
        };
        let token = format!("Bearer {}", authorize_app::request(payload).token);

        let result = validate_app_authorization::request(token.clone());
        assert_eq!(result, "200 OK");

        let result = revoke_app::request(token.clone());
        assert_eq!(result, "200 OK");
    }
}
